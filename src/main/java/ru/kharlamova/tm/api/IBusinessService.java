package ru.kharlamova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.enumerated.Status;
import ru.kharlamova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @NotNull
    E add(@NotNull String userId, @NotNull E entity);

    @NotNull
    Optional<E> findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<E> findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<E> findByName(@NotNull String userId, @NotNull String name);

    void clear(@NotNull String userId);

    @NotNull
    Optional<E> updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    @NotNull
    Optional<E> updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Optional<E> startByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<E> startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<E> startByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Optional<E> finishByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<E> finishById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<E> finishByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Optional<E> changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Optional<E> changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Optional<E> changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    void remove(@NotNull String userId, @NotNull E entity);

    @NotNull
    E removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    E removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    E removeByName(@NotNull String userId, @NotNull String name);

}
