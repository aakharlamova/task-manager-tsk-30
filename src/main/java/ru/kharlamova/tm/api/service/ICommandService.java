package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getListArgumentName();

    @NotNull
    Collection<String> getListCommandName();

    void add(AbstractCommand command);

    @NotNull Collection<AbstractCommand> getArgsCommands();

}
