package ru.kharlamova.tm.exception.authorization;

import ru.kharlamova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }

}
